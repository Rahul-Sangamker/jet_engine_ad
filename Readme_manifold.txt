The objective of this readme document is to explain how to obtain manifold embeddings for the jet engine dataset FD001.
We first create the embeddings by running the Create_manifold.py code.
We then fit the obtained embeddings to the convolutional autoencoder using Manifold_fit.py and we finally use the Manifold_predict.py code the evaluate the performance of the model trained on the manifold embeddings with respect to the model trained using the original data. 
The file json_manifold can be used to change some parameters of the create_manifold.py code. 
The results of the create_manifold.py code are stored in the folder manifold_cache where a json file containing the history of reconstruction error and the best manifold embeddings would be stored.