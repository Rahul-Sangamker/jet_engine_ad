"""
This module is used to define multiple unsupervised classification 
algorithms like k-means, DB scan, and Isolation Forests independently
to train on identifying anomalies within time series data.

Routines in this Module:
------------------------
preprocess_data: Preprocesses input time series data either in .txt or .csv
kmeans_fit : Trains a kmeans model on the input data
isolation_forests_fit: Trains an isolation forest model on input data
dbscan_fit: Trains a DB scan model on the input data
"""

import warnings
warnings.filterwarnings('ignore')
from sklearn.cluster import KMeans
from sklearn.ensemble import IsolationForest
from sklearn.cluster import DBSCAN
from sklearn import manifold


def kmeans_fit(train_data, n_clusters = 1):
    
    """
    This function is used to train a k-means model
    :param train_data: Standardized input time series dataset
    :param n_clusters: number of clusters desired for k-means clustering
    :return: A k-means model fit on input data
    """
    kmeans = KMeans(n_clusters)
    kmeans.fit(train_data)
    return kmeans


def isolation_forests_fit(train_data, n_estimators=100, max_samples='auto', contamination=float(.15), max_features=1.0, bootstrap=False, n_jobs=-1, verbose=0):
    
    """
    This function is used to train an isolation forests model
    :param train_data: Standardized input time series dataset
    :param n_estimators: The number of base estimators in the ensemble
    :param max_samples: The number of samples to draw from train_data to train each base estimator
    :param contamination: proportion of outliers in the dataset
    :param max_features: The number of features to draw from train_data to train each base estimator
    :param bootstrap: If True, individual trees are fit on random subsets of the training
    data sampled with replacement. If False, sampling without replacement is performed.
    :param verbose: Controls the verbosity of the tree building process
    :return: An Isolation Forest model fit on input data
    """
    
    clf=IsolationForest(n_estimators = n_estimators, max_samples = max_samples, contamination = contamination, max_features= max_features, bootstrap= bootstrap, verbose = verbose)
    
    clf.fit(train_data)

    return clf
  
    
def dbscan_fit(train_data, eps = .4, metric= "euclidean", min_samples = 5, n_jobs = -1):
    
    """
    This function is used to train a DB scan model
    :param train_data: Standardized input time series dataset
    :param eps: The maximum distance between two samples for one
    to be considered as in the neighborhood of the other
    :param metrics: The metric to use when calculating distance
    between instances in a feature array.
    :param min_samples: The number of samples (or total weight)
    in a neighborhood for a point to be considered as a core point
    :return: predictions of outliers as a numpy array
    """
    outlier_detection = DBSCAN(eps = eps, metric= metric, min_samples = min_samples)

    model = outlier_detection.fit(train_data)
    clusters = model.labels_
    
    return clusters

def manifold_fit(train_data, n_neighbors = 100, n_components = 5, method = 'standard'):
    
    model = manifold.LocallyLinearEmbedding(n_neighbors = n_neighbors, n_components = n_components,
                                method = method).fit(train_data)
    return model 