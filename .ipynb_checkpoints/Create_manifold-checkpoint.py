#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
Anomaly Detection: Create_manifold.py

This module is used to create embeddings for the Jet engine dataset using the manifold learning process.

Routines in this module:
preprocess_data: Preprocesses input time series data either in .txt or .csv
create_manifold_embeddings: Creates embeddings using manifold learning on data
"""
import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import numpy as np
import json
import copy
from plot_utils import *
from additional_algos_fit import *

def preprocess_data(dataset, feature_cols, feature_names):

    """
    :param dataset: Input time series dataset
    :param feature_cols: Sensor tag columns within dataset
    :param feature_names: Names of input features
    :return: Standardized input data as numpy array
    
    """

    # Check if dataset is txt or csv and read it as pandas dataframe
    if dataset.endswith('.csv'):
        df_master_data = pd.read_csv(dataset, usecols=feature_names,
                                     header='infer')
    elif dataset.endswith('.txt'):
        df_master_data = pd.read_csv(dataset, usecols=feature_cols, sep=" ",
                                     header=None)

    # Convert input dataframe to numpy array for processing 
    np_master_data = df_master_data.values
    scaler = MinMaxScaler()

    # Standardize data using MinMax Scaler
    temp = copy.deepcopy(np_master_data[:,0])
    np_master_data2 = scaler.fit_transform(np_master_data[:,1:])
    np_master_data = np.insert(np_master_data2,0, temp, axis = 1)
    return np_master_data, df_master_data


def create_manifold_embeddings(train_data, n_neighbors = [100], n_components = 5, method = 'modified', eigen_solver = "dense"):

    """
    :param train_data: Input time series dataset
    :param n_neighbors: list of number of neighbors to consider 
                        for each point
    :param n_components: number of coordinates for the manifold
    :param method: Standard, Hessian, modified or Ltsa
    :param eigen_solver: Auto, dense or arpack
    :return: Corresponding manifold embeddings for input data 
    """
    recon_error = {}
    n_embed = {}
    
    for i in n_neighbors:
        
        clf  = manifold.LocallyLinearEmbedding(n_neighbors = i, n_components = n_components,
                                method = method, eigen_solver = "dense")
        temp_train_data = copy.deepcopy(train_data)
        embeddings = clf.fit_transform(temp_train_data)
        recon_error[i] = clf.reconstruction_error_
        n_embed[i] = embeddings 
        
        
#     with open(direc + "/reconstruction_error.json","w") as r:
#         json.dump(recon_error, r)
    
    least_error = min(recon_error.values())
    
    
    for i in sorted(recon_error.keys()):
        if recon_error[i] == least_error:
#             with open(direc + "/Most_efficient_embedding.txt", "w") as m:
#                 np.savetxt(m, n_embed[i])
            return list(n_embed[i]), recon_error
 
    
    
if __name__ == '__main__':


    #Read json data schema
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations from json file
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['train_data_source']
    feature_cols = [0] + config['input_data']['feature_measurements']
    feature_names = config['input_data']['feature_names']
    model_dir_path = config['model_dir']
    train_data, df_train_data = preprocess_data(input_data, feature_cols,
                                                feature_names)
    
    # Read data source and directory locations from json file
    jsonfile2 =  r'C:/Users/RahulSangamker/jet_engine/json_manifold/Manifold_parameters.json'
    json_file2 = open(jsonfile2)
    config = json.load(json_file2)
    direc = config['output_dir']
    n_neighbors = config["Nearest_neighbors_range"]
    n_components = config["No_of_components"]
    method = config["method"]
    eigen_solver = config["eigen_solver"]
    f_embeddings = []  
    recon_error_f = {}
    
    # We create manifold embeddings for each engine separately and store them all in a separate file
    
    for i in range(1,101):
        
        x = train_data[np.where(train_data[:,0]==i)][:,1:]
        most_efficient_embedding, recon_error = create_manifold_embeddings(x, n_neighbors = range(n_neighbors[0], n_neighbors[1], n_neighbors[2]), n_components=n_components , method = method, eigen_solver = eigen_solver)
        recon_error_f[i] = recon_error
        f_embeddings = f_embeddings + most_efficient_embedding
        
    with open(direc + "/reconstruction_error.json","w") as r:
        json.dump(recon_error_f, r)

    with open(direc + "/Most_efficient_embedding.txt", "w") as m:
        np.savetxt(m, f_embeddings)

    recon_error_file = open(direc + "/reconstruction_error.json")
    recon_error = json.load(recon_error_file)


#Plotting the reconstruction error for the created embeddings against Nearest neighbors

    for i in range(1,5):
        plt.figure(i)
        
        plt.plot(recon_error[str(i)].keys(), recon_error[str(i)].values())
        plt.xlabel('N-Nearest Neighbors')
        plt.ylabel('Reconstruction_error')
        plt.title( "Reconstruction error for Engine {}".format(i))
        plt.show()