#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
This module is used to create confusion matrix for comparing the results
of different models and plot them accordingly. It also contains routines
to perform Principal component analysis and visualize the predictions of 
multiple models to identify how the anomalies identified differ from model
to model using a 2D plot

Routines in this Module:
------------------------
preprocess_data: Preprocesses input time series data either in .txt or .csv
kmeans_predict: Loads pretrained kmeans model to return predictions on data  
isofor_predict: Loads pretrained Isolation Forests model to return predictions on data
dbscan_predict: Loads pretrained DB scan model to return predictions on data

"""
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
import matplotlib.pyplot as plt
import copy
from IPython.display import display

def compute_confusionm(y_true, y_pred, title= None, plot = True):

    cm = confusion_matrix(y_true, y_pred)
    
    if plot:
        tot_anom = list(y_true).count(1)
        tot_norm = list(y_true).count(0)
        labels = ['True Neg out of\n{} Non anomalies\n'.format(tot_norm),'False Pos','False Neg','True Pos out of\n{} True anomalies\n'.format(tot_anom)]
        categories = ['Zero', 'One']

        plot_confusion_matrix(np.array(cm), 
                              group_names=labels,
                              categories=categories, 
                              cmap='binary', title = title)
    return cm


def plot_confusion_matrix(cf,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''


    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names)==cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten()/np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels,group_counts,group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0],cf.shape[1])


    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        #Accuracy is sum of diagonal divided by total observations
        accuracy  = np.trace(cf) / float(np.sum(cf))

        #if it is a binary confusion matrix, show some more stats
        if len(cf)==2:
            #Metrics for Binary Confusion Matrices
            precision = cf[1,1] / sum(cf[:,1])
            recall    = cf[1,1] / sum(cf[1,:])
            f1_score  = 2*precision*recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy,precision,recall,f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""


    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize==None:
        #Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks==False:
        #Do not show categories if xyticks is False
        categories=False


    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf,annot=box_labels,fmt="",cmap=cmap,cbar=cbar,xticklabels=categories,yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)
    
    if title:
        plt.title(title)
    
    plt.show()
    
    
def plot_models(data, feature_names, model_name = [], preds = [], describe_pca = False):

    '''
    This function will make a plot of train_data by mapping it to a 2D plot and then highlight
    the anomalous points in the same using 2D PCA embeddings. It can also be used to perform and
    visualize principal component analysis on the input data.
    Arguments
    ---------
    data:                  Standardized input time series dataset
    
    feature_names:         List of sensor data column names in time series dataset
    
    model_name:            List containing the names of the model predictions to be plotted
    
    describe_pca:          If True, performs a PCA on data and plots a PCA plot. Also lists
                           the feature_names and weights of the attributes which contributed
                           the most to the first two Principal components
    
    '''


    if len(model_name)==0 and describe_pca == False:
        return ("No model name given")


    if describe_pca:
        names = feature_names
        x = data
        scaler = StandardScaler()
        pca = PCA()
        pipeline = make_pipeline(scaler, pca)
        pipeline.fit(x)
        # Plot the principal components against their inertia
        features = range(pca.n_components_)
        _ = plt.figure(figsize=(15, 5))
        _ = plt.bar(features, pca.explained_variance_)
        _ = plt.xlabel('PCA feature')
        _ = plt.ylabel('Variance')
        _ = plt.xticks(features)
        _ = plt.title("Importance of the Principal Components based on inertia")
        plt.show()

        def create_importance_dataframe(pca, original_num_df):

            # Change pcs components ndarray to a dataframe
            importance_df  = pd.DataFrame(pca.components_)

            # Assign columns
            importance_df.columns  = original_num_df

            # Change to absolute values
            importance_df =importance_df.apply(np.abs)

            # Transpose
            importance_df=importance_df.transpose()

            # Change column names again

            ## First get number of pcs
            num_pcs = importance_df.shape[1]

            ## Generate the new column names
            new_columns = [f'PC{i}' for i in range(1, num_pcs + 1)]

            ## Now rename
            importance_df.columns  =new_columns

            # Return importance df
            return importance_df

        # Call function to create importance df
        importance_df  = create_importance_dataframe(pipeline.named_steps['pca'], feature_names)


        # Sort depending on PC of interest

        ## PC1 top 10 important features
        pc1_top_10_features = importance_df['PC1'].sort_values(ascending = False)[:10]
        print(), print(f'PC1 top 10 features are \n')
        display(pc1_top_10_features)

        ## PC2 top 10 important features
        pc2_top_10_features = importance_df['PC2'].sort_values(ascending = False)[:10]
        print(), print(f'PC2 top 10 features are \n')
        display(pc2_top_10_features)

    #Iterate over the various models and plot the corresponding 2D plots with anomalies highlighted
    if len(model_name) > 0:

        model = [i.lower() for i in model_name]
        pca = PCA(2)
        pca.fit(data)
        res = pd.DataFrame(pca.transform(data))  

        for model_index in range(len(model)):

            plt.figure(model_index) 
            pred = copy.deepcopy(preds[model_index])
            outlier_index = [i for i in range(len(pred)) if pred[i] == 1]
            plt.title(model[model_index])
            plt.scatter(res[0], res[1], c='green', s=20,label="normal points")
            plt.scatter(res.iloc[outlier_index,0],res.iloc[outlier_index,1], c='green',s=20,  edgecolor="red",label="predicted outliers")
            plt.legend(loc="upper right")

        plt.show()
