import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.plot_utils import visualize_reconstruction_error
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder

data_dir_path = './data'
model_dir_path = './models'
"""
machine_data = pd.read_csv(data_dir_path +
                       '/machine_temperature_system_failure.csv',
                           header='infer')
"""
machine_data = pd.read_csv(data_dir_path + '/train_FD001.txt', sep=" ",
                           header=None)
machine_data = machine_data.drop(columns=[26, 27])
operational_modes = 3
total_sensors = 21
operational_settings_columns_names = ["OpSet" + str(i) for i in range(0,
                                                                      operational_modes)]
sensor_measure_columns_names = ["SensorMeasure" + str(i) for i in range(1,
                                                                        total_sensors + 1)]
input_file_column_names = ['unit_number'] + ['cycles'] + \
                          operational_settings_columns_names + \
                          sensor_measure_columns_names
machine_data.columns = input_file_column_names
machine_np_data = machine_data.iloc[:, 5:26].values
scaler = MinMaxScaler()
machine_np_data = scaler.fit_transform(machine_np_data)
print(machine_np_data.shape)

model = Conv1DAutoEncoder()

# fit the data and save model into model_dir_path

model.fit(machine_np_data, model_dir_path=model_dir_path,
          estimated_negative_sample_ratio=0.9)

# load back the model saved in model_dir_path detect anomaly
model.load_model(model_dir_path)
anomaly_information = model.anomaly(machine_np_data)
reconstruction_error = []
for idx, (is_anomaly, dist) in enumerate(anomaly_information):
    print('# ' + str(idx) + ' is ' + ('abnormal' if is_anomaly else
                                      'normal') + ' (dist: ' + str(dist)
          + ')')
    reconstruction_error.append(dist)

visualize_reconstruction_error(reconstruction_error, model.threshold)
