#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8

"""
Anomaly Detection:

This module uses two different Deep Learning (DL) algorithms namely
Feedforward Autoencoder and Convolutional 1D Autoencoder independently to
detect anomalies within time series data. Feature Importance is estimated
using the Hybrid Rank methodology published in the paper "Early stabilizing
feature importance for TensorFlow deep neural networks", Heaton, Jeffrey &
McElwee, Steven & Fraley, James & Cannady, James. (2017). Early stabilizing
feature importance for TensorFlow deep neural networks.
10.1109/IJCNN.2017.7966442. The paper describes using a hybrid combination of
three different feature importance estimation methods namely, Pearson
product-moment correlation coefficient ranking, Garson 'Connection Weights'
ranking and Input Perturbation based ranking.

Classes in this Module:
keras_anomaly_detection: Provides a library of Deep Learning algorithms
including Feedforward Autoencoder, Convolutional 1D Autoencoder,
LSTM Autoencoders.

Ranking: Normalizes feature importance rank estimated using different
techniques

InputPerturbationRank: Enables estimation of feature importance rank based
on Input Perturbation technique.

HybridRank: Enables estimation of final feature importance rank from trained DL
models using Input Perturbation method, Correlation coefficient rank and
Connection Weights ranking.

Routines in this module:




"""


from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.plot_utils import \
    visualize_reconstruction_error
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
from sklearn import metrics
from tabulate import tabulate
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import csv
import codecs
import random
import math


ENCODING = 'utf-8'


class Ranking(object):
    def __init__(self, sensor_names):
        self.sensor_names = sensor_names

    def _normalize(self, x, impt):
        impt = impt / sum(impt)
        impt = list(zip(impt, self.sensor_names, range(x.shape[1])))
        impt.sort(key=lambda x: -x[0])
        return impt


class InputPerturbationRank(Ranking):
    def __init__(self, sensor_names):
        super(InputPerturbationRank, self).__init__(sensor_names)

    @staticmethod
    def _raw_rank(x, y, network):
        impt = np.zeros(x.shape[1])
        for i in range(x.shape[1]):
            hold = np.array(x[:, i])
            np.random.shuffle(x[:, i])
            # Handle both TensorFlow and SK-Learn models.
            if ('tensorflow' or 'keras') in str(type(network)).lower():
                pred = list(network.predict(x, as_iterable=True))
            else:
                pred = network.predict(x)
            rmse = metrics.mean_squared_error(y, pred)
            impt[i] = rmse
            x[:, i] = hold
        return impt

    def rank(self, x, y, network):
        impt = self._raw_rank(x, y, network)
        return self._normalize(x, impt)


class HybridRank(InputPerturbationRank):
    def __init__(self, sensor_names):
        super(HybridRank, self).__init__(sensor_names)

    @staticmethod
    def weight_vector(network):
        # weights = network.get_weights('dnn/hiddenlayer_0/weights')
        weights = network.model.get_weights()
        if 'feedforward' in str(type(network)).lower():
            weights = np.power(weights[0], 2)
        elif 'conv1d' in str(type(network)).lower():
            weights_transposed = weights[2].transpose()
            weights = np.power(weights_transposed, 2)
        weights = np.sum(weights, axis=1)
        weights = np.sqrt(weights)
        weights = weights / sum(weights)
        return weights

    @staticmethod
    def simple_vector(x, y):
        impt = []
        for i in range(x.shape[1]):
            c = np.corrcoef(x[:, i], y[:])
            if math.isnan(abs(c[1, 0])):
                c = 0
                impt.append(c)
            else:
                impt.append(abs(c[1, 0]))
        return np.array(impt)

    def rank(self, x, y, network):
        p_rank = self._raw_rank(x, y, network)
        w_rank = self.weight_vector(network)
        c_rank = self.simple_vector(x, y)
        d = (np.std(p_rank / sum(p_rank)))
        impt = w_rank + (p_rank * d) + (c_rank * (1.0 - d))
        result = self._normalize(x, impt)
        return result


def preprocess_data(dataset, sensor_tags):
    if dataset.endswith('.csv'):
        machine_data = pd.read_csv(dataset)
    elif dataset.endswith('.txt'):
        machine_data = pd.read_csv(dataset, usecols=sensor_tags, sep=" ",
                                   header=None)
    machine_data.columns = ['TOT_TEMP_FAN', 'TOT_TEMP_LPC_OUT',
                            'TOT_TEMP_HPC_OUT', 'TOT_TEMP_LPT_OUT',
                            'PSI_FAN_INLET', 'TOT_PSI_BYPASS_DUCT',
                            'TOT_PSI_HPC_OUT', 'FAN_SPEED', 'CORE_SPEED',
                            'ENGINE_PSI_RATIO', 'STAT_PSI_HPC_OUT',
                            'RATIO_FUEL_FLOW_STAT_PSI_HPC_OUT',
                            'CORRECTED_FAN_SPEED', 'CORRECTED_CORE_SPEED',
                            'BYPASS_RATIO', 'BURNER_FUEL_AIR_RATIO',
                            'BLEED_ENTHALPY', 'DEMANDED_FAN_SPEED',
                            'DEMANDED_CORRECTED_FAN_SPEED',
                            'HPT_COOLANT_BLEED', 'LPT_COOLANT_BLEED']
    machine_np_data = machine_data.values
    scaler = MinMaxScaler()
    machine_np_data = scaler.fit_transform(machine_np_data)
    return machine_np_data, machine_data


def feature_importance_estimator(x, y, trained_model, sensor_names,
                                 model_name, results_dir):
    ranker = HybridRank(sensor_names)
    with codecs.open(results_dir + "rank_stability_" + model_name + '.csv', \
            "w", ENCODING) as fh:
        writer = csv.writer(fh, lineterminator='\n')
        writer.writerow(['HybridRank Score', 'Sensor Tag', 'Index'])
        random.seed(42)
        r_test = ranker.rank(x, y, trained_model)
        r_test.sort(key=lambda x: x[2])
        r_test_list = [list(i) for i in r_test]
        print(tabulate(r_test_list,
                       headers=['Hybrid Rank Score', 'Sensor Tag',
                                'Index'],
                       tablefmt='grid'))
        for score, tag, index in r_test:
            writer.writerow([score, tag, index])
    return r_test_list


if __name__ == '__main__':

    import time
    import json

    # Data Acquisition
    jsonfile = r'/home/radkris/anomaly_prediction/json/' \
               r'ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Data Initialization
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['data_source']
    sensor_cols = config['input_data']['sensor_measurements']
    sensor_data, df_sensor_data = preprocess_data(input_data, sensor_cols)

    # Initialize Feedforward Autoencoder Model Instance
    model_1 = FeedForwardAutoEncoder()

    # Initialize Model Configuration from input file
    feedforward_dae_kwargs = {
        "encoding_dim": config['feedforward_dae_config']['encoding_dim'],
        "encoder_1_actv": config['feedforward_dae_config']['encoder_1_actv'],
        "encoder_1_l1": config['feedforward_dae_config']['encoder_1_l1'],
        "encoder_2_actv": config['feedforward_dae_config']['encoder_2_actv'],
        "decoder_1_actv": config['feedforward_dae_config']['decoder_1_actv'],
        "decoder_2_actv": config['feedforward_dae_config']['decoder_2_actv'],
        "epochs": config['feedforward_dae_config']['epochs'],
        "batch_size": config['feedforward_dae_config']['batch_size']
    }

    start_time_1 = time.time()

    # Fit the data and save model into model_dir_path
    model_1.fit(sensor_data, feedforward_dae_kwargs["encoding_dim"],
              feedforward_dae_kwargs["encoder_1_actv"],
              feedforward_dae_kwargs["encoder_1_l1"],
              feedforward_dae_kwargs["encoder_2_actv"],
              feedforward_dae_kwargs["decoder_1_actv"],
              feedforward_dae_kwargs["decoder_2_actv"],
              feedforward_dae_kwargs["epochs"],
              feedforward_dae_kwargs["batch_size"],
              model_dir_path=output_dir,
              estimated_negative_sample_ratio=0.9)

    # Load back the model saved in model_dir_path detect anomaly
    model_1.load_model(output_dir)
    anomaly_information_1 = model_1.anomaly(sensor_data)
    reconstruction_error_1 = []
    data_character_1 = []
    for idx, (is_anomaly, dist) in enumerate(anomaly_information_1):
        print('# ' + str(idx) + ' is ' + ('abnormal' if is_anomaly else
                                          'normal') + ' (dist: ' + str(dist)
              + ')')
        reconstruction_error_1.append(dist)
        data_character_1.append(is_anomaly*1)
    anomaly_data_1 = {'data_character': data_character_1,
                      'reconstruction_error': reconstruction_error_1}
    df_anomaly_information_1 = pd.DataFrame(anomaly_data_1)
    df_sensor_data_anomalies_1 = pd.concat([df_sensor_data,
                                            df_anomaly_information_1], axis=1)
    df_anomaly_information_1.to_csv(output_dir +
                                    'anomaly_information_feedforward.csv')
    df_sensor_data_anomalies_1.to_csv(output_dir +
                                      'sensor_data_with_anomalies_feedforward.csv')
    print("---------------------------- Completed Training of Feedforward "
          "Autoencoder -------------------------")
    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_1))

    ##########################################################
    # Initialize Convolutional 1D Autoencoder Model Instance #
    ##########################################################

    model_2 = Conv1DAutoEncoder()

    # Initialize Model Configuration from input file
    conv1d_dae_kwargs = {
        "num_filters": config['conv1d_dae_config']['num_filters'],
        "kernel_size": config['conv1d_dae_config']['kernel_size'],
        "activation_1": config['conv1d_dae_config']['activation_1'],
        "activation_2": config['conv1d_dae_config']['activation_2'],
        "epochs": config['conv1d_dae_config']['epochs'],
        "batch_size": config['conv1d_dae_config']['batch_size']
    }

    start_time_2 = time.time()

    # Fit the data and save model into model_dir_path
    model_2.fit(sensor_data,
                conv1d_dae_kwargs["num_filters"],
                conv1d_dae_kwargs["kernel_size"],
                conv1d_dae_kwargs["activation_1"],
                conv1d_dae_kwargs["activation_2"],
                conv1d_dae_kwargs["epochs"],
                conv1d_dae_kwargs["batch_size"],
                model_dir_path=output_dir,
                estimated_negative_sample_ratio=0.9)

    # Load back the model saved in model_dir_path detect anomaly
    model_2.load_model(output_dir)
    anomaly_information_2 = model_2.anomaly(sensor_data)
    reconstruction_error_2 = []
    data_character_2 = []
    for idx, (is_anomaly, dist) in enumerate(anomaly_information_2):
        print('# ' + str(idx) + ' is ' + ('abnormal' if is_anomaly else
                                          'normal') + ' (dist: ' + str(dist)
              + ')')
        reconstruction_error_2.append(dist)
        data_character_2.append(is_anomaly*1)
    anomaly_data_2 = {'data_character': data_character_2,
                      'reconstruction_error': reconstruction_error_2}
    df_anomaly_information_2 = pd.DataFrame(anomaly_data_2)
    df_sensor_data_anomalies_2 = pd.concat([df_sensor_data,
                                            df_anomaly_information_2], axis=1)
    df_sensor_data_anomalies_2.to_csv(output_dir +
                                      'sensor_data_with_anomalies_conv1dautoencoder.csv')
    df_anomaly_information_2.to_csv(output_dir +
                                    'anomaly_information_conv1dautoencoder.csv')

    print("---------------------------- Completed Training of "
          "Convolutional-1D Autoencoder -------------------------")
    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_2))

    # Feature Importance Calculation
    names = list(df_sensor_data.columns)
    x_inputs = sensor_data
    y_feedforward_dae = np.asarray(data_character_1)
    y_conv1d_dae = np.asarray(data_character_2)
    feature_importance_model_1 = \
        feature_importance_estimator(x_inputs, y_feedforward_dae, model_1,
                                     names,
                                     model_name='feed_forward_dae',
                                     results_dir=output_dir)
    feature_importance_model_2 = \
        feature_importance_estimator(x_inputs, y_conv1d_dae, model_2, names,
                                     model_name='conv1d_dae',
                                     results_dir=output_dir)

    # Visualize Reconstruction Error Plots
    ax1 = plt.subplot(211)
    visualize_reconstruction_error(reconstruction_error_1,
                                         model_1.threshold)
    ax1.set_title('Anomaly Detection on NASA Jet Engine Fleet using '
                 'Feedforward Autoencoder')
    ax2 = plt.subplot(212, sharex=ax1)
    visualize_reconstruction_error(reconstruction_error_2,
                                         model_2.threshold)
    ax2.set_title('Anomaly Detection on NASA Jet Engine Fleet using '
              'Convolutional-1D Autoencoder')
    plt.tight_layout()
    plt.show()
