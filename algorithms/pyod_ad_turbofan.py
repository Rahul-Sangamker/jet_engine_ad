from __future__ import division
from __future__ import print_function
from pyod.models.auto_encoder import AutoEncoder
from pyod.utils.data import evaluate_print
from sklearn.preprocessing import MinMaxScaler
from pyod.utils.data import generate_data

import os
import sys
import json
import pandas as pd
from sklearn.model_selection import train_test_split

# temporary solution for relative imports in case pyod is not installed
# if pyod is installed, no need to use the following line
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname("__file__"), '..')))


def preprocess_data(dataset, sensor_tags):
    if dataset.endswith('.csv'):
        machine_data = pd.read_csv(dataset)
    elif dataset.endswith('.txt'):
        machine_data = pd.read_csv(dataset, usecols=sensor_tags, sep=" ",
                                   header=None)
    machine_np_data = machine_data.values
    scaler = MinMaxScaler()
    machine_np_data = scaler.fit_transform(machine_np_data)
    return machine_np_data


if __name__ == "__main__":
    jsonfile = r'/home/radkris/ad_blueprint/json/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Data Initialization
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['data_source']
    sensor_cols = config['input_data']['sensor_measurements']
    sensor_data = preprocess_data(input_data, sensor_cols)

    contamination = 0.1
    n_train = 16504  # number of training points
    n_test = 4127  # number of testing points
    n_features = len(sensor_cols)  # number of features

    # Generate train test samples
    X_train, y_train, X_test, y_test = \
        generate_data(n_train=n_train,
                      n_test=n_test,
                      n_features=n_features,
                      contamination=contamination,
                      random_state=42)

    # train AutoEncoder detector
    clf_name = 'AutoEncoder'
    clf = AutoEncoder(epochs=100, contamination=contamination)

    X_train, X_test = train_test_split(sensor_data, test_size=0.2)

    clf.fit(X_train)

    # get the prediction labels and outlier scores of the training data
    X_train_pred = clf.labels_  # binary labels (0: inliers, 1: outliers)
    X_train_scores = clf.decision_scores_  # raw outlier scores

    # get the prediction on the test data
    X_test_pred = clf.predict(X_test)  # outlier labels (0 or 1)
    X_test_scores = clf.decision_function(X_test)  # outlier scores

    # evaluate and print the results
    print("\nOn Training Data:")
    evaluate_print(clf_name, X_train, X_train_scores)
    print("\nOn Test Data:")
    evaluate_print(clf_name, X_test, X_test_scores)
