"""
Author: Radhakrishnan Poomari
@author: rk@xformics.com
"""

import h2o
import os
import pandas as pd
import time
from h2o.estimators.deeplearning import H2OAutoEncoderEstimator


import matplotlib.pyplot as plt

# Read time series dataset and split it into test and train

def train_test_split(dataset, train_start_index, train_end_index):
    df_master = h2o.import_file(dataset)
    df_train = df_master[train_start_index:train_end_index, :]
    df_test = df_master[(train_end_index + 1):, :]
    return df_train, df_test

"""
def train_test_split(dataset, train_start_index, train_end_index):

    machine_data = h2o.import_file(dataset)
    df_machine_data = machine_data.as_data_frame(use_pandas=True)
    #machine_data = df_machine_data.drop(columns=[26, 27])

    operational_modes = 3
    total_sensors = 21
    operational_settings_columns_names = ["OpSet" + str(i) for i in range(0,
                                                                          operational_modes)]
    sensor_measure_columns_names = ["SensorMeasure" + str(i) for i in range(1,
                                                                            total_sensors + 1)]
    input_file_column_names = ['unit_number'] + ['cycles'] + \
                              operational_settings_columns_names + \
                              sensor_measure_columns_names
    machine_data.columns = input_file_column_names
    machine_np_data = df_machine_data.iloc[:, 3:26]
    machine_np_data = h2o.H2OFrame(machine_np_data
                                   )
    df_train = machine_np_data[train_start_index:train_end_index, :]
    df_test = machine_np_data[(train_end_index + 1):, :]
    return df_train, df_test
"""

# Reconstruction Error Calculation

def recon_error_calc(df_h2o_test, model):
    recon_error = model.anomaly(df_h2o_test)
    recon_error_per_feature = model.anomaly(df_h2o_test, per_feature=True)
    df_recon_error = recon_error.as_data_frame(use_pandas=True)
    df_recon_error_per_feature = recon_error_per_feature.as_data_frame(
        use_pandas=True)
    #df_pandas_test = df_h2o_test.as_data_frame(use_pandas=True)
    #df_recon_error['DateTime'] = pd.to_datetime(df_pandas_test['DateTime'])
    #df_recon_error_per_feature['DateTime'] = pd.to_datetime(df_pandas_test[
    #                                                            'DateTime'])
    return  df_recon_error, df_recon_error_per_feature

def save_model(modelpath, model):
    json_model_performance = json.dumps(str(model.model_performance(

    )._metric_json), ensure_ascii=False)
    dict_model_performance = json.loads(json_model_performance)
    with open(modelpath + 'Model_Metrics.json', 'w') as outfile:
        json.dump(dict_model_performance, outfile)
    df_model_summary = model.summary().as_data_frame()
    df_model_summary.to_csv(modelpath + 'Model_Summary.csv')
    json_model = model.save_model_details()
    bin_model = h2o.save_model(model=model, path=modelpath, force=True)
    return json_model, bin_model, df_model_summary

def save_top_features(csvpath, model):
    list_top_features = model.varimp()
    df_top_features = pd.DataFrame(list_top_features, columns=['Top '
                                                               'Features',
                                                               'Relative '
                                                               'Importance',
                                                               'Scaled '
                                                               'Importance',
                                                               'Percentage'])
    df_top_features.to_csv(csvpath + 'Top_Features.csv')
    return df_top_features

def deep_autoencoder(dataset):
    start_time = time.time()
    df_h2o_train, df_h2o_test = train_test_split(dataset, train_start_index,
                                                 train_end_index)
    model = H2OAutoEncoderEstimator(activation=config['analyticSettings'][
        'activation'], seed=config['analyticSettings']['seed'],
                                    hidden=config['analyticSettings'][
                                        'hidden'], epochs=config[
            'analyticSettings']['epochs'], reproducible=True,
                                    standardize=True, l2=config[
            'analyticSettings']['l2_regularization'],
                                    nesterov_accelerated_gradient=bool(config[
            'analyticSettings']['nesterov_accelerated_gradient']),
                                    missing_values_handling='skip')
    model.train(x=df_h2o_train.names, training_frame=df_h2o_train)
    df_h2o_test_recon = model.predict(df_h2o_test)
    df_h2o_train_recon = model.predict(df_h2o_train)
    df_h2o_test_recon = df_h2o_test_recon.as_data_frame(use_pandas=True)
    df_h2o_train_recon = df_h2o_train_recon.as_data_frame(use_pandas=True)
    print("------------------------------- Model Runtime: %s seconds "
          "-------------------------------" % (time.time() - start_time))
    anomaly_score_test, anomaly_score_test_per_feature = recon_error_calc(
        df_h2o_test, model)
    anomaly_score_train, anomaly_score_train_per_feature = recon_error_calc(
        df_h2o_train, model)
    #df_h2o_train_recon['DateTime'] = anomaly_score_train['DateTime']
    #df_h2o_test_recon['DateTime'] = anomaly_score_test['DateTime']

    top_features = save_top_features(outputpath, model)
    save_model(outputpath, model)
    anomaly_score_train.to_csv(outputpath + 'Train_Anomaly_Scores.csv')
    anomaly_score_test.to_csv(outputpath + 'Test_Anomaly_Scores.csv')
    anomaly_score_train_per_feature.to_csv(outputpath +
                                           'Train_Anomaly_Scores_per_feature.csv')
    anomaly_score_test_per_feature.to_csv(outputpath +
                                           'Test_Anomaly_Scores_per_feature.csv')
    df_h2o_train_recon.to_csv(outputpath + 'Reconstructed_Training_Set.csv')
    df_h2o_test_recon.to_csv(outputpath + 'Reconstructed_Test_Set.csv')
    return df_h2o_train, df_h2o_test, anomaly_score_train, anomaly_score_test

def visualize_reconstruction_error(reconstruction_error, threshold):
    plt.plot(reconstruction_error, marker='o', ms=3.5, linestyle='',
             label='Point')

    plt.hlines(threshold, xmin=0, xmax=len(reconstruction_error)-1, colors="r", zorder=100, label='Threshold')
    plt.legend()
    plt.title("Reconstruction error")
    plt.ylabel("Reconstruction error")
    plt.xlabel("Data point index")
    plt.show()


if __name__ == '__main__':
    import sys
    import json
    h2o.init()
    jsonfile = sys.argv[1]
    json_file = open(jsonfile)
    config = json.load(json_file)
    train_start_index = config['dataSplit']['train_start_index']
    train_end_index = config['dataSplit']['train_end_index']
    dataset = config['dataSource']
    path = config['workDir']
    outputpath = config['outputDir']
    #os.chdir(path)
    train_data, test_data, train_anomaly_scores, test_anomaly_scores = \
        deep_autoencoder(
        dataset)
    plt.scatter(train_anomaly_scores.index,
                train_anomaly_scores, s=7)
    plt.show()
