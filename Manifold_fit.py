 #!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
Anomaly Detection: Manifold_fit.py

This module fits (trains) a different Deep Learning (DL) algorithm namely
Convolutional 1D Autoencoder to detect anomalies within time series data
which is created using manifold learning.


"""
import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import numpy as np
import json
from plot_utils import *
from additional_algos_fit import *
import time

    
if __name__ == '__main__':

#     Read json data schema
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json_manifold/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations from json file
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['train_data_source']
    model_dir_path = config['model_dir']
    train_data = np.loadtxt(input_data)
    #Loading anomaly predictions for convolutional autoencoder model

    model_2 = Conv1DAutoEncoder()

    # Initialize Model Configuration from input file for
    conv1d_dae_kwargs = {
        "num_filters": config['conv1d_dae_config']['num_filters'],
        "kernel_size": config['conv1d_dae_config']['kernel_size'],
        "activation_1": config['conv1d_dae_config']['activation_1'],
        "activation_2": config['conv1d_dae_config']['activation_2'],
        "epochs": config['conv1d_dae_config']['epochs'],
        "batch_size": config['conv1d_dae_config']['batch_size']
    }

    # Start timer to estimate elapsed time for training (C1DAE)model
    start_time_2 = time.time()

    # Fit C1DAE to pre-processed data and save model into model directory
    model_2.fit(train_data,
                conv1d_dae_kwargs["num_filters"],
                conv1d_dae_kwargs["kernel_size"],
                conv1d_dae_kwargs["activation_1"],
                conv1d_dae_kwargs["activation_2"],
                conv1d_dae_kwargs["epochs"],
                conv1d_dae_kwargs["batch_size"],
                model_dir_path=model_dir_path,
                estimated_negative_sample_ratio=0.9)

    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_2))
    
