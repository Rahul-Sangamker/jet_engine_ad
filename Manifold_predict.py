import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import numpy as np
import json
from plot_utils import *
from additional_algos_fit import *
import time



#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
This module loads multipled trained algorithms like k-means, DB scan,
and Isolation Forests independently to detect anomalies within time 
series data. It also uses anomaly predictions from a trained 
convolutional autoencoder to compare the results of the aforementioned
algorithms

Routines in this Module:
------------------------
preprocess_data: Preprocesses input time series data either in .txt or .csv


"""
import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import numpy as np
import json
from plot_utils import *
from additional_algos_fit import *

def preprocess_data(dataset, feature_cols, feature_names):

    """
    :param dataset: Input time series dataset
    :param feature_cols: Sensor tag columns within dataset
    :param feature_names: Names of input features
    :return: Standardized input data as numpy array
    """

    # Check if dataset is txt or csv and read it as pandas dataframe
    if dataset.endswith('.csv'):
        df_master_data = pd.read_csv(dataset, usecols=feature_names,
                                     header='infer')
    elif dataset.endswith('.txt'):
        df_master_data = pd.read_csv(dataset, usecols=feature_cols, sep=" ",
                                     header=None)
        # Assign column headers if headers are not present
        df_master_data.columns = feature_names

    # Convert input dataframe to numpy array for processing 
    np_master_data = df_master_data.values
    scaler = MinMaxScaler()

    # Standardize data using MinMax Scaler
    np_master_data = scaler.fit_transform(np_master_data)
    return np_master_data, df_master_data

if __name__ == '__main__':

#     Read json data schema
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations from json file

    input_data = config['train_data_source']
    feature_cols = config['input_data']['feature_measurements']
    feature_names = config['input_data']['feature_names']
    model_dir_path = config['model_dir']
    train_data, df_train_data = preprocess_data(input_data, feature_cols,
                                                feature_names)
    
    #Loading anomaly predictions for convolutional autoencoder model

    model_2 = Conv1DAutoEncoder()
    model_2.load_model(model_dir_path)
    
    
    convanom_information = model_2.anomaly(train_data)
    convanom = list(convanom_information)
    convauto_preds = [i[0].astype(int) for i in convanom]


    
    
#     Read json data schema
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json_manifold/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations of the input manifold embeddings from json file
    input_data = config['train_data_source']
    model_dir_path = config['model_dir']
    train_data_2 = np.loadtxt(input_data)
    
    #Loading anomaly predictions for convolutional autoencoder model

    model_2 = Conv1DAutoEncoder()
    model_2.load_model(model_dir_path)    
    convanom_information = model_2.anomaly(train_data_2)
    convanom = list(convanom_information)
    convauto_preds_manifold = [i[0].astype(int) for i in convanom]   
     

    
    """The following line returns the confusion matrix and 
    plots the confusion matrix if plot attribute is set to True"""
    
    compute_confusionm(convauto_preds, convauto_preds_manifold, plot = True,title = "Confusion matrix for Manifold Embeddings") 
    
    """The following lines plot the anomalies obtained using 
    convolutional autoencoders on the original data and manifold
    embeddings. PCA components of original data will be plotted
    if describe_pca is set to True"""

    plot_models(train_data, feature_names, model_name = ["convolutional autoencoders on og data","convolutional autoencoders on Manifold data"], preds = [convauto_preds, convauto_preds_manifold], describe_pca = False)