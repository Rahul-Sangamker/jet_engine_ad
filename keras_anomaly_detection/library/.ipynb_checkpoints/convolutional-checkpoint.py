import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
from keras.layers import Conv1D, GlobalMaxPool1D, Dense
from keras.models import model_from_json
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
import numpy as np

from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(1234)


class Conv1DAutoEncoder(object):
    model_name = 'con1d-auto-encoder'
    VERBOSE = 1

    def __init__(self):
        self.model = None
        self.time_window_size = None
        self.metric = None
        self.threshold = 5.0
        self.config = None

    @staticmethod
    def create_model(time_window_size, n_filters, kernel_size,
                     activation_1, activation_2, metric):
        model = Sequential()
        model.add(Conv1D(filters=n_filters, kernel_size=kernel_size,
                         padding='same',
                         activation=activation_1,
                         input_shape=(time_window_size, 1)))
        model.add(GlobalMaxPool1D())

        model.add(Dense(units=time_window_size, activation=activation_2))

        model.compile(optimizer='adam', loss='mean_squared_error', metrics=[
            metric])
        print(model.summary())
        return model

    @staticmethod
    def get_architecture_file_path(model_dir_path):
        return os.path.join(model_dir_path, Conv1DAutoEncoder.model_name +
                            '-architecture.json')

    @staticmethod
    def get_weight_file_path(model_dir_path):
        return os.path.join(model_dir_path, Conv1DAutoEncoder.model_name +
                            '-weights.h5')

    @staticmethod
    def get_config_file_path(model_dir_path):
        return os.path.join(model_dir_path, Conv1DAutoEncoder.model_name +
                            '-config.npy')

    def load_model(self, model_dir_path):
        config_file_path = Conv1DAutoEncoder.get_config_file_path(
            model_dir_path)
        self.config = np.load(config_file_path,allow_pickle=True).item()
        self.metric = self.config['metric']
        self.time_window_size = self.config['time_window_size']
        self.threshold = self.config['threshold']
        architecture_file_path = Conv1DAutoEncoder.get_architecture_file_path(
            model_dir_path)
        self.model = model_from_json(open(architecture_file_path, 'r').read())
        weight_file_path = Conv1DAutoEncoder.get_weight_file_path(
            model_dir_path)
        self.model.load_weights(weight_file_path)

    def fit(self, dataset, filters, kernel_shape, activ_fn_1, activ_fn_2,
            epochs, batch_size, model_dir_path, validation_split=0.1,
            metric='mean_absolute_error',
            estimated_negative_sample_ratio=0.9):

        self.time_window_size = dataset.shape[1]
        self.metric = metric

        input_timeseries_dataset = np.expand_dims(dataset, axis=2)

        weight_file_path = self.get_weight_file_path(model_dir_path=model_dir_path)
        architecture_file_path = self.get_architecture_file_path(model_dir_path)
        checkpoint = ModelCheckpoint(weight_file_path)
        self.model = self.create_model(self.time_window_size,
                                       filters, kernel_shape, activ_fn_1,
                                       activ_fn_2, metric=self.metric)
        open(architecture_file_path, 'w').write(self.model.to_json())
        history = self.model.fit(x=input_timeseries_dataset, y=dataset,
                                 batch_size=batch_size, epochs=epochs,
                                 verbose=self.VERBOSE,
                                 validation_split=validation_split,
                                 callbacks=[checkpoint]).history
        self.model.save_weights(weight_file_path)

        scores = self.predict(dataset)
        scores.sort()
        cut_point = int(estimated_negative_sample_ratio * len(scores))
        self.threshold = scores[cut_point]

        print('estimated threshold is ' + str(self.threshold))

        self.config = dict()
        self.config['time_window_size'] = self.time_window_size
        self.config['metric'] = self.metric
        self.config['threshold'] = self.threshold
        config_file_path = self.get_config_file_path(model_dir_path=model_dir_path)
        np.save(config_file_path, self.config)

        return history

    def predict(self, timeseries_dataset):
        input_timeseries_dataset = np.expand_dims(timeseries_dataset, axis=2)
        target_timeseries_dataset = self.model.predict(
            x=input_timeseries_dataset)
        dist = np.linalg.norm(timeseries_dataset -
                              target_timeseries_dataset, axis=-1)
        return dist

    def anomaly(self, timeseries_dataset, threshold=None):
        if threshold is not None:
            self.threshold = threshold

        dist = self.predict(timeseries_dataset)
        return zip(dist >= self.threshold, dist)
