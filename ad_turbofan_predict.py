#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
Anomaly Detection: ad_blueprint_predict.py
------------------------------------------

This module loads two different trained Deep Learning (DL) algorithms namely
Feedforward Autoencoder and Convolutional 1D Autoencoder independently to
detect anomalies within time series data.

Classes in this Module:
-----------------------
keras_anomaly_detection: Provides a library of Deep Learning algorithms
including Feedforward Autoencoder, Convolutional 1D Autoencoder,
LSTM Autoencoders.


Routines in this Module:
------------------------
preprocess_data: Preprocesses input time series data either in .txt or .csv

feature_importance_estimator: Uses the HybridRank Class to calculate feature
importance rank for all input features.
"""

from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.plot_utils import \
    visualize_reconstruction_error
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
from tabulate import tabulate
from feature_importance import HybridRank
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import csv
import codecs
import random

ENCODING = 'utf-8'

def preprocess_data(dataset, feature_cols, feature_names):

    """
    :param dataset: Input time series dataset
    :param feature_cols: Sensor tag columns within dataset
    :param feature_names: Names of input features
    :return:
    """

    # Check if dataset is txt or csv and read it as pandas dataframe
    if dataset.endswith('.csv'):
        df_master_data = pd.read_csv(dataset, usecols=feature_names,
                                     header='infer')
    elif dataset.endswith('.txt'):
        df_master_data = pd.read_csv(dataset, usecols=feature_cols, sep=" ",
                                     header=None)
        # Assign column headers if headers are not present
        df_master_data.columns = feature_names

    # Convert input dataframe to numpy array for processing using tensorflow
    np_master_data = df_master_data.values
    scaler = MinMaxScaler()

    # Standardize data using MinMax Scalar
    np_master_data = scaler.fit_transform(np_master_data)
    return np_master_data, df_master_data


def feature_importance_estimator(x, y, trained_model, feature_names,
                                 model_name, results_dir):
    ranker = HybridRank(feature_names)
    with codecs.open(results_dir + "rank_stability_" + model_name + '.csv', \
            "w", ENCODING) as fh:
        writer = csv.writer(fh, lineterminator='\n')
        writer.writerow(['HybridRank Score', 'Feature Name', 'Index'])
        random.seed(42)
        r_test = ranker.rank(x, y, trained_model)
        #r_test.sort(key=lambda x: x[2])
        r_test_list = [list(i) for i in r_test]
        print(tabulate(r_test_list,
                       headers=['Hybrid Rank Score', 'Feature Name',
                                'Index'],
                       tablefmt='grid'))
        for score, tag, index in r_test:
            writer.writerow([score, tag, index])
    return r_test_list


if __name__ == '__main__':

    import time
    import json

    # Data Acquisition
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Data Initialization
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['test_data_source']
    feature_cols = config['input_data']['feature_measurements']
    feature_names = config['input_data']['feature_names']
    model_dir_path = config['model_dir']
    train_data, df_train_data = preprocess_data(input_data, feature_cols,
                                                feature_names)

    # Initialize Feedforward Autoencoder Model Instance
    model_1 = FeedForwardAutoEncoder()

    # Load back the model saved in model_dir_path detect anomaly
    model_1.load_model(model_dir_path)
    start_time_1 = time.time()
    anomaly_information_1 = model_1.anomaly(train_data)
    reconstruction_error_1 = []
    data_character_1 = []
    for idx, (is_anomaly, dist) in enumerate(anomaly_information_1):
        print('# ' + str(idx) + ' is ' + ('abnormal' if is_anomaly else
                                          'normal') + ' (dist: ' + str(dist)
              + ')')
        reconstruction_error_1.append(dist)
        data_character_1.append(is_anomaly * 1)
    anomaly_data_1 = {'data_character': data_character_1,
                      'reconstruction_error': reconstruction_error_1}
    df_anomaly_information_1 = pd.DataFrame(anomaly_data_1)
    df_anomalies_1 = pd.concat([df_train_data,
                                df_anomaly_information_1], axis=1)
    df_anomaly_information_1.to_csv(output_dir +
                                    'anomaly_information_feedforward.csv')
    df_anomalies_1.to_csv(output_dir +
                                      'anomalies_feedforward.csv')
    print("---------------------------- Completed Training of Feedforward "
          "Autoencoder -------------------------")
    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_1))

    ##########################################################
    # Initialize Convolutional 1D Autoencoder Model Instance #
    ##########################################################
    model_2 = Conv1DAutoEncoder()
    start_time_2 = time.time()

    # Load back the model saved in model_dir_path detect anomaly
    model_2.load_model(model_dir_path)
    anomaly_information_2 = model_2.anomaly(train_data)
    reconstruction_error_2 = []
    data_character_2 = []
    for idx, (is_anomaly, dist) in enumerate(anomaly_information_2):
        print('# ' + str(idx) + ' is ' + ('abnormal' if is_anomaly else
                                          'normal') + ' (dist: ' + str(dist)
              + ')')
        reconstruction_error_2.append(dist)
        data_character_2.append(is_anomaly * 1)
    anomaly_data_2 = {'data_character': data_character_2,
                      'reconstruction_error': reconstruction_error_2}
    df_anomaly_information_2 = pd.DataFrame(anomaly_data_2)
    df_anomalies_2 = pd.concat([df_train_data,
                                df_anomaly_information_2], axis=1)
    df_anomalies_2.to_csv(output_dir +
                                      'anomalies_conv1dautoencoder.csv')
    df_anomaly_information_2.to_csv(output_dir +
                                    'anomaly_information_conv1dautoencoder.csv')

    print("---------------------------- Completed Training of "
          "Convolutional-1D Autoencoder -------------------------")
    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_2))

    # Feature Importance Calculation
    names = list(df_train_data.columns)
    x_inputs = train_data
    y_feedforward_dae = np.asarray(data_character_1)
    y_conv1d_dae = np.asarray(data_character_2)
    feature_importance_model_1 = \
        feature_importance_estimator(x_inputs, y_feedforward_dae, model_1,
                                     names,
                                     model_name='feed_forward_dae',
                                     results_dir=output_dir)
    feature_importance_model_2 = \
        feature_importance_estimator(x_inputs, y_conv1d_dae, model_2, names,
                                     model_name='conv1d_dae',
                                     results_dir=output_dir)

    # Visualize Reconstruction Error Plots
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    visualize_reconstruction_error(reconstruction_error_1,
                                   model_1.threshold)
    ax1.set_title('Anomaly Graph using Feedforward Autoencoder')
    fig2 = plt.figure(2)
    ax2 = fig2.add_subplot(111)
    ax2.set_ylim([0, 0.01])
    visualize_reconstruction_error(reconstruction_error_2,
                                   model_2.threshold)
    ax2.set_title('Anomaly Graph using Convolutional-1D Autoencoder')
    plt.tight_layout()
    plt.show()
