#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
This module loads multipled trained algorithms like k-means, DB scan,
and Isolation Forests independently to detect anomalies within time 
series data. It also uses anomaly predictions from a trained 
convolutional autoencoder to compare the results of the aforementioned
algorithms

Routines in this Module:
------------------------
preprocess_data: Preprocesses input time series data either in .txt or .csv
kmeans_predict: Loads pretrained kmeans model to return predictions on data  
isofor_predict: Loads pretrained Isolation Forests model to return predictions on data
dbscan_predict: Loads pretrained DB scan model to return predictions on data

"""
import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import numpy as np
import json
from plot_utils import *
from additional_algos_fit import *
from manifold import create_manifold_embeddings

def preprocess_data(dataset, feature_cols, feature_names):

    """
    :param dataset: Input time series dataset
    :param feature_cols: Sensor tag columns within dataset
    :param feature_names: Names of input features
    :return: Standardized input data as numpy array
    """

    # Check if dataset is txt or csv and read it as pandas dataframe
    if dataset.endswith('.csv'):
        df_master_data = pd.read_csv(dataset, usecols=feature_names,
                                     header='infer')
    elif dataset.endswith('.txt'):
        df_master_data = pd.read_csv(dataset, usecols=feature_cols, sep=" ",
                                     header=None)
        # Assign column headers if headers are not present
        df_master_data.columns = feature_names

    # Convert input dataframe to numpy array for processing 
    np_master_data = df_master_data.values
    scaler = MinMaxScaler()

    # Standardize data using MinMax Scaler
    np_master_data = scaler.fit_transform(np_master_data)
    return np_master_data, df_master_data



# Write a function that calculates distance between each point and the centroid of the closest cluster
def kmeans_predict(model, data, outliers_fraction=0.20):
    
    """
    :param data: Standardized input time series dataset
    :param model: Represents the pretrained model 
    :param outliers_fraction: Percentage of anomalies in the dataset
    :return: Binary anomaly predictions as a numpy array
    """

    def getDistanceByPoint(data, model):
        
        """ Function that calculates the distance between a point and centroid of a cluster, 
                returns the distances in pandas series"""
        
        distance = []
        
        for i in range(0,len(data)):
            
            Xa = np.array(data[i])
            Xb = model.cluster_centers_[model.labels_[i]-1]
            distance.append(np.linalg.norm(Xa-Xb))
            
        return pd.Series(distance)
    
    
    # get the distance between each point and its nearest centroid. The biggest distances are considered as anomaly
    distance = getDistanceByPoint(data, model)
    # number of observations that equate to the outlier fraction of the entire data set
    number_of_outliers = int(outliers_fraction*len(distance))
    # Take the minimum of the largest outlier fractions of the distances as the threshold
    threshold = distance.nlargest(number_of_outliers).min()
    data = (distance >= threshold).astype(int)
    data_array = data.values.flatten()

    
    return data_array


def isofor_predict(model, data):
    
    """
    :param data: Standardized input time series dataset
    :param model: Represents the pretrained IF model 
    :return: Binary anomaly predictions as a numpy array
    """
    
    pred = model.predict(data)
    temp = np.where(pred== 1,0, pred)
    pred = np.where(temp== -1, 1, temp)
    
    return pred

def dbscan_predict(pred):
    
    """
    This method justs maps the outputs of the predictions
    given by the dbscan_fit method to zeros and ones
    :param data: Standardized input time series dataset
    :param model: Represents the pretrained DB scan model 
    :return: Binary anomaly predictions as a numpy array
    """
    
    temp = np.where(pred != -1, 0, pred)
    pred = np.where(pred == -1, 1, temp)
    
    return pred.tolist()


    
if __name__ == '__main__':

    
#     Read json data schema
    jsonfile = r'C:/Users/RahulSangamker/jet_engine/json/ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations from json file
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['train_data_source']
    feature_cols = config['input_data']['feature_measurements']
    feature_names = config['input_data']['feature_names']
    model_dir_path = config['model_dir']
    train_data, df_train_data = preprocess_data(input_data, feature_cols,
                                                feature_names)

    #Loading anomaly predictions for convolutional autoencoder model

    model_2 = Conv1DAutoEncoder()
    model_2.load_model(model_dir_path)
    
    
    convanom_information = model_2.anomaly(train_data)
    convanom = list(convanom_information)
    convauto_preds = [i[0].astype(int) for i in convanom]
    

#     #Loading anomaly predictions for k-means clustering model

#     kmeans = kmeans_fit(train_data, n_clusters =2)
#     kmeans_predictions = kmeans_predict(kmeans, train_data, outliers_fraction=0.20)
    
    
#     #Loading anomaly predictions for Isolation Forests model
    
#     isofor = isolation_forests_fit(train_data, n_estimators=200, max_samples='auto', contamination=float(.20), max_features=3, bootstrap=False, verbose=0)
#     isolation_forests_predictions = isofor_predict(isofor, train_data)
    
    
# #     Loading anomaly predictions for DB scan algorithm
    
#     dbscan_temp_preds = dbscan_fit(train_data, eps = .2, metric = "euclidean", min_samples = 5)
#     dbscan_predictions = dbscan_predict(dbscan_temp_preds)
    

    
    
#     """The following lines returns the confusion matrix and 
#     plots the confusion matrix if plot attribute is set to True"""
    
#     compute_confusionm(convauto_preds, isolation_forests_predictions, plot = True,title = "Confusion matrix for Isolation Forests") #Confusion matrix for isolation forest predictions
        
#     n_clusters = 2
#     compute_confusionm(convauto_preds, kmeans_predictions, plot = True, title = "Confusion matrix for kmeans with k = {}".format(n_clusters))  #Confusion matrix for k-means predictions
    
#     compute_confusionm(convauto_preds, dbscan_predictions, plot = True,title = "Confusion matrix for DB scan clustering") #Confusion matrix for DB scan predictions
    
    
    
#     """ plot_models function helps us visualize the differences
#     between which points are being 
#     identified as anomalous by various models """
    
#     plot_models(train_data, feature_names, model_name = ["convolutional autoencoders","isolation forest", "K-means", "DB Scan"], preds = [convauto_preds, isolation_forests_predictions, kmeans_predictions, dbscan_predictions], describe_pca = True)


#####################################################
#     model_2.load_model(model_dir_path)

#     manifold_embeddings = create_manifold_embeddings(train_data, n_neighbors = 50)
#     convanom_information_2 = model_2.anomaly(manifold_embeddings)
#     convanom_manifold = list(convanom_information_2)
#     convauto_preds_manifold = [i[0].astype(int) for i in convanom_manifold]
#     convauto_preds_manifold = [i[0].astype(int) for i in convanom_preds_manifold]
#     compute_confusionm(convauto_preds, convauto_preds_manifold, plot = True,title = "Confusion matrix for Isolation Forests") 

#     plot_models(train_data, feature_names, model_name = ["AD with Conv Autoencoders on Training Data","AD with Conv Autoencoders on Manifold embeddings"], preds = [convauto_preds, convauto_preds_manifold], describe_pca = True)
    
#####################################################

manifold_embeddings = create_manifold_embeddings(train_data, n_neighbors = 50)
kmeans = kmeans_fit(train_data, n_clusters =2)
kmeans_predictions = kmeans_predict(kmeans, train_data, outliers_fraction=0.20)

kmeans2 = kmeans_fit(manifold_embeddings, n_clusters = 2)
kmeans_predictions_manifold = kmeans_predict(kmeans2, manifold_embeddings, outliers_fraction = 0.20 )

cm1 = compute_confusionm(convauto_preds, kmeans_predictions_manifold, plot = True, title = "Confusion matrix for kmeans using manifold")
cm2 = compute_confusionm(convauto_preds, kmeans_predictions, plot = True, title = "Confusion matrix for kmeans using original training data")
cm3 = compute_confusionm(kmeans_predictions, kmeans_predictions_manifold, title = "cm3")

