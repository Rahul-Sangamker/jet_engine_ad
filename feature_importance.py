#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com


"""
Model Interpretation: feature_importance.py
-------------------------------------------

This module contains libraries for interpreting tensorflow/keras based Deep
Learning (DL) models based on state of the art HybridRank algorithm. Feature
Importance is estimated using the Hybrid Rank methodology published in the
paper "Early stabilizing feature importance for TensorFlow deep neural
networks", Heaton, Jeffrey & McElwee, Steven & Fraley, James & Cannady,
James. (2017). Early stabilizing feature importance for TensorFlow deep
neural networks. 10.1109/IJCNN.2017.7966442. The paper describes using a
hybrid combination of three different feature importance estimation methods
namely, Pearson product-moment correlation coefficient ranking, Garson
'Connection Weights' ranking and Input Perturbation based ranking.

Classes in this Module:
-----------------------
Ranking: Normalizes feature importance rank estimated using different
techniques

InputPerturbationRank: Enables estimation of feature importance rank based
on Input Perturbation technique.

HybridRank: Enables estimation of final feature importance rank from trained DL
models using Input Perturbation method, Correlation coefficient rank and
Connection Weights ranking.

Routines in this module:
------------------------
_normalize: Normalizes an estimated feature importance ranking

_raw_rank: Estimates raw feature importance score based on Input
Perturbation technique. The input orders are shuffled and mean-squared error is
calculated for the resulting model. Wrong input values presented for each
expected target while the column maintains the same distribution. No adverse
effect on DNN other than the feature being perturbed. Its strength is that
no re-training is needed and the weakness is its model dependent

rank(InputPerturbation): Calculates feature importance rank of a model using
raw rank score

weight_vector: A feature importance estimation method using a tensorflow
model. Primarily uses weights from inputs to hidden layer of a DL model. It
is a simplified version of Garson "Connection weights" technique - G. Garson.
“Interpreting neural-network connection weights”. Artificial Intelligence
Expert 6, 1991. 47–51.

simple_vector: Estimates feature importance based on Pearson product-moment
correlation coefficients. It calculates features independently and hence
may have weakness for univariate analysis and if presence of redundant
features.

rank(HybridRank): Estimates feature importance based on a hybrid combination of
Correlation Coefficient Rank, Input Perturbation Rank and Connection Weights
Rank. Hybrid algorithm uses the weight rank plus a weighted sum of input
perturbation and correlation coefficient. The standard deviation of the
normalized perturbation rank is used to balance input perturbation and the
correlation coefficient rank. It requires fewer training iterations for
large feature sets.

"""

from sklearn import metrics
import numpy as np
import math


class Ranking(object):
    def __init__(self, sensor_names):
        self.sensor_names = sensor_names

    def _normalize(self, x, impt):

        """
        :param x: Input timeseries dataset
        :param impt: packages estimated importance score with input features
        :return: importance score for each feature (sorted list of floats)
        """
        impt = impt / sum(impt)
        impt = list(zip(impt, self.sensor_names, range(x.shape[1])))
        impt.sort(key=lambda x: -x[0])
        return impt


class InputPerturbationRank(Ranking):
    def __init__(self, sensor_names):
        super(InputPerturbationRank, self).__init__(sensor_names)

    @staticmethod
    def _raw_rank(x, y, network):

        """
        :param x: Input timeseries dataset
        :param y: Target feature indicating if a datapoint is normal/abnormal
        :param network: Trained tensorflor/Keras DL model (Currently this can
        be a Feedforward Autoencoder or Convolutional 1-D Autoencoder
        :return: Feature importance rank for each input feature using Input
        Perturbation technique.
        """
        impt = np.zeros(x.shape[1])
        for i in range(x.shape[1]):
            hold = np.array(x[:, i])
            np.random.shuffle(x[:, i])

            # Handle both TensorFlow and SK-Learn models.
            if ('tensorflow' or 'keras') in str(type(network)).lower():
                pred = list(network.predict(x, as_iterable=True))
            else:
                pred = network.predict(x)
            rmse = metrics.mean_squared_error(y, pred)
            impt[i] = rmse
            x[:, i] = hold
        return impt

    def rank(self, x, y, network):

        """
        :param x: Input time series dataset
        :param y: Target feature indicating if a datapoint is normal/abnormal
        :param network: Trained tensorflor/Keras DL model (Currently this can
        be a Feedforward Autoencoder or Convolutional 1-D Autoencoder
        :return: Normalized feature importance rank for each input feature
        using Input Perturbation technique.
        """
        impt = self._raw_rank(x, y, network)
        return self._normalize(x, impt)


class HybridRank(InputPerturbationRank):
    def __init__(self, sensor_names):
        super(HybridRank, self).__init__(sensor_names)

    @staticmethod
    def weight_vector(network):

        """
        :param network: Trained tensorflor/Keras DL model (Currently this can
        be a Feedforward Autoencoder or Convolutional 1-D Autoencoder
        :return: Feature importance rank for each input feature using
        Connection Weights technique.
        """
        # weights = network.get_weights('dnn/hiddenlayer_0/weights')

        # Get model weights from all layers of DNN model
        weights = network.model.get_weights()

        # Check architecture of trained DNN model
        if 'feedforward' in str(type(network)).lower():
            # Use weights of 0th layer (input to hidden layer)
            weights = np.power(weights[0], 2)
        elif 'conv1d' in str(type(network)).lower():
            # Use weights of 2nd layer (hidden layer that maps out to input
            # features
            weights_transposed = weights[2].transpose() # Transpose to match
            # dimension of output from Connection Weights and Correlation
            # Coefficients
            weights = np.power(weights_transposed, 2)
        weights = np.sum(weights, axis=1)
        weights = np.sqrt(weights)
        weights = weights / sum(weights)
        return weights

    @staticmethod
    def simple_vector(x, y):

        """
        :param x: Input time series dataset
        :param y: Target feature indicating if a datapoint is normal/abnormal
        :return: Importance score for each feature using Correlation
        coefficient analysis of input features
        """
        impt = []
        for i in range(x.shape[1]):
            c = np.corrcoef(x[:, i], y[:]) # Calculate correlation coefficient
            if math.isnan(abs(c[1, 0])):
                c = 0 # If coefficients are nan values, set them to zero
                impt.append(c)
            else:
                impt.append(abs(c[1, 0]))
        return np.array(impt)

    def rank(self, x, y, network):

        """
        :param x: Input time series dataset
        :param y: Target feature indicating if a datapoint is normal/abnormal
        :param network: Trained tensorflor/Keras DL model (Currently this can
        be a Feedforward Autoencoder or Convolutional 1-D Autoencoder
        :return: Feature importance rank based on hybrid rank technique
        """
        p_rank = self._raw_rank(x, y, network)
        w_rank = self.weight_vector(network)
        c_rank = self.simple_vector(x, y)
        d = (np.std(p_rank / sum(p_rank))) # Perturbation rank is weighted
        # by standard deviation d
        impt = w_rank + (p_rank * d) + (c_rank * (1.0 - d)) # Correlation
        # coefficient is weighted by 1 – d
        result = self._normalize(x, impt)
        return result
