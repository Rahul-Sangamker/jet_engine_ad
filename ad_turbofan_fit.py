#!"~/anaconda3/envs/tensorflow/bin/python"
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
Anomaly Detection: ad_blueprint_fit.py

This module fits (trains) two different Deep Learning (DL) algorithms namely
Feedforward Autoencoder and Convolutional 1D Autoencoder independently to
detect anomalies within time series data.

Classes in this Module:
keras_anomaly_detection: Provides a library of Deep Learning algorithms
including Feedforward Autoencoder, Convolutional 1D Autoencoder,
LSTM Autoencoders.

Routines in this module:
preprocess_data: Preprocesses input time series data either in .txt or .csv
"""

from sklearn.preprocessing import MinMaxScaler
from keras_anomaly_detection.library.feedforward import FeedForwardAutoEncoder
from keras_anomaly_detection.library.convolutional import Conv1DAutoEncoder
import pandas as pd
import tensorflow as tf
import os

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

def preprocess_data(dataset, feature_cols, feature_names):

    """
    :param dataset: Input time series dataset
    :param feature_cols: Sensor tag columns within dataset
    :param feature_names: Names of input features
    :return: Standardized input data as numpy array
    """

    # Check if dataset is txt or csv and read it as pandas dataframe
    if dataset.endswith('.csv'):
        df_master_data = pd.read_csv(dataset, usecols=feature_names,
                                     header='infer')
    elif dataset.endswith('.txt'):
        df_master_data = pd.read_csv(dataset, usecols=feature_cols, sep=" ",
                                     header=None)
        # Assign column headers if headers are not present
        df_master_data.columns = feature_names

    # Convert input dataframe to numpy array for processing using tensorflow
    np_master_data = df_master_data.values
    scaler = MinMaxScaler()

    # Standardize data using MinMax Scalar
    np_master_data = scaler.fit_transform(np_master_data)
    return np_master_data, df_master_data


if __name__ == '__main__':

    import time
    import json
    # Read json data schema
    jsonfile = r'C:\Users\rahul\xformics_RS_AD\jet_engine\json\ad_data_schema_turbofan.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Read data source and directory locations from json file
    work_dir = config['work_dir']
    output_dir = config['output_dir']
    input_data = config['train_data_source']
    feature_cols = config['input_data']['feature_measurements']
    feature_names = config['input_data']['feature_names']
    model_dir_path = config['model_dir']
    train_data, df_train_data = preprocess_data(input_data, feature_cols,
                                                feature_names)

    ############################################################
    # Initialize Feedforward Autoencoder (FFAE) Model Instance #
    ############################################################
    model_1 = FeedForwardAutoEncoder()

    # Initialize Model Configuration from input file
    feedforward_dae_kwargs = {
        "encoding_dim": config['feedforward_dae_config']['encoding_dim'],
        "encoder_1_actv": config['feedforward_dae_config']['encoder_1_actv'],
        "encoder_1_l1": config['feedforward_dae_config']['encoder_1_l1'],
        "encoder_2_actv": config['feedforward_dae_config']['encoder_2_actv'],
        "decoder_1_actv": config['feedforward_dae_config']['decoder_1_actv'],
        "decoder_2_actv": config['feedforward_dae_config']['decoder_2_actv'],
        "epochs": config['feedforward_dae_config']['epochs'],
        "batch_size": config['feedforward_dae_config']['batch_size']
    }


    # Start timer to estimate elapsed time for training Feedforward
    # Autoencoder model
    start_time_1 = time.time()

    # Fit FFAE to pre-processed data and save model into model directory
    model_1.fit(train_data, feedforward_dae_kwargs["encoding_dim"],
                feedforward_dae_kwargs["encoder_1_actv"],
                feedforward_dae_kwargs["encoder_1_l1"],
                feedforward_dae_kwargs["encoder_2_actv"],
                feedforward_dae_kwargs["decoder_1_actv"],
                feedforward_dae_kwargs["decoder_2_actv"],
                feedforward_dae_kwargs["epochs"],
                feedforward_dae_kwargs["batch_size"],
                model_dir_path=model_dir_path,
                estimated_negative_sample_ratio=0.9)

    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_1))

    ##################################################################
    # Initialize Convolutional 1D Autoencoder (C1DAE) Model Instance #
    ##################################################################
    model_2 = Conv1DAutoEncoder()

    # Initialize Model Configuration from input file for
    conv1d_dae_kwargs = {
        "num_filters": config['conv1d_dae_config']['num_filters'],
        "kernel_size": config['conv1d_dae_config']['kernel_size'],
        "activation_1": config['conv1d_dae_config']['activation_1'],
        "activation_2": config['conv1d_dae_config']['activation_2'],
        "epochs": config['conv1d_dae_config']['epochs'],
        "batch_size": config['conv1d_dae_config']['batch_size']
    }

    # Start timer to estimate elapsed time for training (C1DAE)model
    start_time_2 = time.time()

    # Fit C1DAE to pre-processed data and save model into model directory
    model_2.fit(train_data,
                conv1d_dae_kwargs["num_filters"],
                conv1d_dae_kwargs["kernel_size"],
                conv1d_dae_kwargs["activation_1"],
                conv1d_dae_kwargs["activation_2"],
                conv1d_dae_kwargs["epochs"],
                conv1d_dae_kwargs["batch_size"],
                model_dir_path=model_dir_path,
                estimated_negative_sample_ratio=0.9)

    print("------------------ Model Training time: %s seconds "
          "------------------" % (time.time() - start_time_2))
